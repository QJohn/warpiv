#!/bin/bash
NUM_PROCS=
MPI_EXEC=mpiexec
# get the command line arguments
ARGS=()
for i in "$@"
do
    case $i in
        -n=*)
            NUM_PROCS=`echo $i | sed 's/[-a-zA-Z0-9]*=//'`
            ;;

        -np=*)
            NUM_PROCS=`echo $i | sed 's/[-a-zA-Z0-9]*=//'`
            ;;

        --mpiexec=*)
            MPI_EXEC=`echo $i | sed 's/[-a-zA-Z0-9]*=//'`
            echo "MPI_EXEC=$MPI_EXEC"
            ;;

        --warpvisit-install=*)
            WARPIV_INSTALL=`echo $i | sed 's/[-a-zA-Z0-9]*=//'`
            if [ ! -e "$WARPIV_INSTALL" ]
            then
                echo "ERROR: $i not found."
                exit
            fi
            ;;

        --visit-install=*)
            ARGS+=("$i")
            VISIT_INSTALL=`echo $i | sed 's/[-a-zA-Z0-9]*=//'`
            ;;

        --sim-file=*)
            ARGS+=("$i")
            SIM_FILE=`echo $i | sed 's/[-a-zA-Z0-9]*=//'`
            rm -f $SIM_FILE
            ;;

        --viewer-opts=*)
            ARGS+=("$i")
            VIEWER_OPTS=`echo $i | sed 's/[-a-zA-Z0-9]*=//'`
            ;;


        --gdb)
            GDB=1
            ;;

        --pydb)
            PYDB=1
            ;;

        --massif)
            MASSIF=1
            ;;

        --memcheck)
            MEMCHECK=1
            ;;

        --no-cli)
            NOCLI=1
            ;;

        --cli-memcheck)
            CLI_MEMCHECK=1
            ;;

        --cli)
            CLI=1
            ;;

        --batch)
            ARGS+=("$i")
            CLI=2
            ;;

        --interact)
            ARGS+=("$i")
            GUI=1
            ;;

        --help)
            ARGS+=("$i")
            HELP=1
            ;;

        *)
            ARGS+=("$i")
            ;;

    esac
done


if [[ -z "$WARPIV_INSTALL" ]]
then
    WARPIV_INSTALL=`pwd`
fi


if [[ -n "$HELP" ]]
then
    source ${WARPIV_INSTALL}/WarpIVEnv.sh $@
    python ${WARPIV_INSTALL}/WarpIVCLIMain.py ${ARGS[@]}
    python ${WARPIV_INSTALL}/WarpIVEngineMain.py ${ARGS[@]}
    echo "WarpIV.sh"
    echo "-n or -np : : number of MPI processes to launch"
    echo "--visit-install : : path to visit install, configures the environment for you"
    echo "--mpiexec : : mpiexec command and extra args"
    echo "--gdb : : run the program in gdb"
    echo "--pydb : : run the program in pydb"
    echo "--cli : : start a VisIt CLI process"
    echo "--massif : : profile engine/sim using massif"
    echo "--memcheck : : profile engine/sim using memcheck"
    echo "--cli-memcheck : : profile cli using memcheck"
    echo
    exit
fi


# start the gui
if [[ -n "$GUI" ]] # &&  -e ${VISIT} ]]
then
    echo "starting gui..."
    echo "${VISIT_INSTALL}/bin/visit"
    "${VISIT_INSTALL}/bin/visit" ${VIEWER_OPTS} &
    # if the program crashes then kill the GUI
    #LAUNCHER_PID=$!
    #VISIT_PROC_GROUP=`ps --pid ${LAUNCHER_PID} --no-headers -o  "%r"`
    #trap "{ kill -TERM -${VISIT_PROC_GROUP}; }" SIGINT SIGTERM SIGABRT EXIT
fi

# configure the environment
source ${WARPIV_INSTALL}/WarpIVEnv.sh $@

# start the run
if [[ -n "$CLI" && -z "$NOCLI" ]]
then
    if [[ $CLI_MEMCHECK -eq 1 ]]
    then
        echo "starting cli with memcheck..."
        xterm -geometry 150x50 -e valgrind --tool=memcheck --leak-check=full --show-reachable=yes --num-callers=50 --db-attach=yes python ${WARPIV_INSTALL}/WarpIVCLIMain.py "${ARGS[@]}" &
    else
        echo -n "starting cli..."
        python ${WARPIV_INSTALL}/WarpIVCLIMain.py "${ARGS[@]}" &
    fi
    CLI_PID=$!
    echo $CLI_PID
    if [[ "$CLI" == "1" ]]
    then
        wait $CLI_PID
    else
        trap "echo stopping cli $CLI_PID...; kill $CLI_PID; exit;" SIGHUP SIGINT SIGTERM EXIT
    fi
fi
if [[ -z "$NUM_PROCS" ]]
then
    echo "starting serial run..."
    if [[ $GDB -eq 1 ]]
    then
        echo "starting gdb..."
        echo "run WarpIVEngineMain.py ${ARGS[@]}"
        gdb -ex r --args python ${WARPIV_INSTALL}/WarpIVEngineMain.py ${ARGS[@]}
    elif [[ $PYDB -eq 1 ]]
    then
        echo "starting pydb..."
        pydb WarpIVEngineMain.py ${ARGS[@]}
    elif [[ $MASSIF -eq 1 ]]
    then
        echo "starting with massif..."
        valgrind --tool=massif python ${WARPIV_INSTALL}/WarpIVEngineMain.py "${ARGS[@]}"

    elif [[ $MEMCHECK -eq 1 ]]
    then
        echo "starting with memcheck..."
        #valgrind --tool=memcheck --leak-check=full --show-reachable=yes --num-callers=50 --log-file=memcheck.log python ${WARPIV_INSTALL}/WarpIVEngineMain.py "${ARGS[@]}"
        valgrind --tool=memcheck --leak-check=full --show-reachable=yes --num-callers=50 --db-attach=yes python ${WARPIV_INSTALL}/WarpIVEngineMain.py "${ARGS[@]}"

    else
        python ${WARPIV_INSTALL}/WarpIVEngineMain.py "${ARGS[@]}"
    fi
else
    echo "starting $NUM_PROCS way parallel run..."
    if [[ $GDB -eq 1 ]]
    then
        echo "starting gdb..."
        echo "run WarpIVEngineMain.py ${ARGS[@]}"
        $MPI_EXEC -n $NUM_PROCS xterm -geometry 160x60 -e gdb -ex r --args pyMPI WarpIVEngineMain.py ${ARGS[@]}
    elif [[ $PYDB -eq 1 ]]
    then
        echo "starting pydb..."
        $MPI_EXEC -n $NUM_PROCS xterm -geometry 160x60 -e pydb pyMPI ${WARPIV_INSTALL}/WarpIVEngineMain.py "${ARGS[@]}"
    elif [[ $MASSIF -eq 1 ]]
    then
        $MPI_EXEC -n $NUM_PROCS pyMPI ${WARPIV_INSTALL}/WarpIVEngineMain.py "${ARGS[@]}"
        echo "starting with massif..."
        $MPI_EXEC -n $NUM_PROCS pyMPI valgrind --tool=massif python ${WARPIV_INSTALL}/WarpIVEngineMain.py "${ARGS[@]}"
    elif [[ $MEMCHECK -eq 1 ]]
    then
        echo "starting with memcheck..."
        $MPI_EXEC -n $NUM_PROCS xterm -geometry 150x50 -e valgrind --tool=memcheck --leak-check=full --show-reachable=yes --num-callers=50 --trace-children=no --db-attach=no pyMPI ${WARPIV_INSTALL}/WarpIVEngineMain.py "${ARGS[@]}"
        #$MPI_EXEC -n $NUM_PROCS valgrind --tool=memcheck --leak-check=full --show-reachable=yes --num-callers=50 --log-file=memcheck-%p.log --trace-children=no pyMPI ${WARPIV_INSTALL}/WarpIVEngineMain.py "${ARGS[@]}"
    else
        $MPI_EXEC -n $NUM_PROCS pyMPI ${WARPIV_INSTALL}/WarpIVEngineMain.py "${ARGS[@]}"
    fi
fi
