function(depend_swig input output)
    set(input_file ${CMAKE_CURRENT_SOURCE_DIR}/${input})
    set(output_file ${CMAKE_CURRENT_BINARY_DIR}/${output})
    # custom command to update the dependency file
    add_custom_command(
        OUTPUT ${output_file}
        COMMAND ${swig_cmd} -c++ -python -MM
            -I${CMAKE_CURRENT_BINARY_DIR}
            -I${CMAKE_CURRENT_BINARY_DIR}/..
            ${input_file} | sed -e 's/[[:space:]\\]\\{1,\\}//g' -e '1,2d' > ${output_file}
        MAIN_DEPENDENCY ${input_file}
        COMMENT "Generating dependency file for ${input}...")
    # bootstrap the dependency list
    message(STATUS "Generating initial dependency list for ${input}")
    execute_process(
        COMMAND ${swig_cmd} -c++ -python -MM
            -I${CMAKE_CURRENT_BINARY_DIR}
            -I${CMAKE_CURRENT_BINARY_DIR}/..
            ${input_file}
       COMMAND sed -e s/[[:space:]\\]\\{1,\\}//g -e 1,2d
       OUTPUT_FILE ${output_file})
endfunction()

function(wrap_swig input output depend)
    set(input_file ${CMAKE_CURRENT_SOURCE_DIR}/${input})
    set(output_file ${CMAKE_CURRENT_BINARY_DIR}/${output})
    set(depend_file ${CMAKE_CURRENT_BINARY_DIR}/${depend})
    file(STRINGS ${depend_file} depends)
    add_custom_command(
        OUTPUT ${output_file}
        COMMAND ${swig_cmd} -c++ -python
            -DSWIG_TYPE_TABLE=WarpIV
            -I${CMAKE_CURRENT_SOURCE_DIR}
            -I${CMAKE_CURRENT_BINARY_DIR}
            -o ${output_file} ${input_file}
        MAIN_DEPENDENCY ${input_file}
        DEPENDS ${depend_file} ${depends}
        COMMENT "Generating python bindings for ${input}...")
endfunction()

function(swig_python_module mname)
    set(MultiArgs SOURCES LIBS)
    cmake_parse_arguments(SWIG_MOD "" "" "${MultiArgs}" ${ARGN})
    depend_swig(WarpIV${mname}.i WarpIV${mname}.dep)
    wrap_swig(WarpIV${mname}.i WarpIV${mname}Swig.cxx WarpIV${mname}.dep)
    include_directories(SYSTEM ${PYTHON_INCLUDE_PATH} ${NUMPY_INCLUDE_DIR})
    PYTHON_ADD_MODULE(_WarpIV${mname}
        ${CMAKE_CURRENT_BINARY_DIR}/WarpIV${mname}Swig.cxx ${SWIG_MOD_SOURCES})
    target_link_libraries(_WarpIV${mname} ${PYTHON_LIBRARIES} ${SWIG_MOD_LIBS})
    add_custom_command(TARGET _WarpIV${mname} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
            ${CMAKE_CURRENT_BINARY_DIR}/WarpIV${mname}.py
            ${CMAKE_CURRENT_BINARY_DIR}/../../)
    add_custom_command(TARGET _WarpIV${mname} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
            ${CMAKE_CURRENT_BINARY_DIR}/_WarpIV${mname}.so
            ${CMAKE_CURRENT_BINARY_DIR}/../../)
    install(TARGETS _WarpIV${mname} LIBRARY DESTINATION lib)
    install(FILES WarpIV${mname}.py DESTINATION lib)
endfunction()
