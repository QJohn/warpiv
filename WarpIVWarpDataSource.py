from WarpIVDataSource import WarpIVDataSource
from WarpIVWarpMeshSource import WarpIVWarpMeshSource
from WarpIVWarpParticleSource import WarpIVWarpParticleSource
import warpoptions
warpoptions.ignoreUnknownArgs = True
warpoptions.quietImport = True
warpoptions.init_parser()
import warp

class WarpIVWarpDataSource(WarpIVDataSource):
    """The data source for warp simulations """
    def __init__(self):
        psrc = WarpIVWarpParticleSource()
        msrc = WarpIVWarpMeshSource(psrc)
        super(WarpIVWarpDataSource, self).__init__(psrc, msrc)

    def GetTime(self):
        """return active time"""
        return warp.top.time

    def GetTimeStep(self):
        """return the active time step"""
        return warp.top.it
