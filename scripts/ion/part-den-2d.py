
try:
    pd2_it
except NameError:
    pd2_it = 0
else:
    pd2_it += 1

var = ['Electron-All', 'Carbon-0', 'Proton-0']

for active_var in var:

    if dataBin:
        AddPlot("Pseudocolor", active_var + "/e_magnitude", 1, 0)
        AddOperator("DataBinning",0)
        DataBinningAtts = DataBinningAttributes()
        DataBinningAtts.numDimensions = DataBinningAtts.Two  # One, Two, Three
        DataBinningAtts.dim1BinBasedOn = DataBinningAtts.Z  # X, Y, Z, Variable
        DataBinningAtts.dim1NumBins = int(nz)
        DataBinningAtts.dim2BinBasedOn = DataBinningAtts.X  # X, Y, Z, Variable
        DataBinningAtts.dim2NumBins = int(nx)
        DataBinningAtts.reductionOperator = DataBinningAtts.Count  # Average, Minimum, Maximum, StandardDeviation, Variance, Sum, Count, RMS, PDF
        DataBinningAtts.emptyVal = 0
        DataBinningAtts.outputType = DataBinningAtts.OutputOnInputMesh  # OutputOnBins, OutputOnInputMesh
        SetOperatorOptions(DataBinningAtts, 0)

        # fix pcolor variable
        ChangeActivePlotsVar("operators/DataBinning/2D/" + active_var)
    else:
        AddPlot("Pseudocolor", "grid/" + active_var + "/den", 1, 0)

    # Setup the pseudocolor attributes
    PseudocolorAtts = PseudocolorAttributes()
    PseudocolorAtts.limitsMode = PseudocolorAtts.OriginalData  # OriginalData, CurrentPlot
    PseudocolorAtts.minFlag = 1
    PseudocolorAtts.min = 0
    PseudocolorAtts.maxFlag = 0
    PseudocolorAtts.max = 1
    PseudocolorAtts.colorTableName = "hot"
    PseudocolorAtts.invertColorTable = 0
    PseudocolorAtts.opacityType = PseudocolorAtts.Constant  # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
    PseudocolorAtts.opacityVariable = ""
    PseudocolorAtts.opacity = 0.25
    PseudocolorAtts.pointSize = 0.05
    PseudocolorAtts.pointType = PseudocolorAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
    PseudocolorAtts.pointSizePixels = 2
    SetPlotOptions(PseudocolorAtts)

    # Rotate the plot using Transform to swap the x/z axis in the 2D plot
    AddOperator("Transform", 0)
    TransformAtts = TransformAttributes()
    TransformAtts.doRotate = 1
    TransformAtts.rotateOrigin = (0, 0, 0)
    TransformAtts.rotateAxis = (0, 1, 0)
    TransformAtts.rotateAmount = 90
    TransformAtts.rotateType = TransformAtts.Deg  # Deg, Rad
    TransformAtts.doScale = 1
    TransformAtts.scaleOrigin = (0, 0, 0)
    TransformAtts.scaleX = 1
    TransformAtts.scaleY = 1
    TransformAtts.scaleZ = -1
    SetOperatorOptions(TransformAtts, 0)

    # Project the data to 2D
    AddOperator("Project", 0)
    ProjectAtts = ProjectAttributes()
    ProjectAtts.projectionType = ProjectAtts.XZCartesian  # ZYCartesian, XZCartesian, XYCartesian, XRCylindrical, YRCylindrical, ZRCylindrical
    SetOperatorOptions(ProjectAtts, 0)

    setCam2D()
    setAtts2D()
    DrawPlots()
    saveWin('%s-part-den-2d-%04d.png'%(active_var, pd2_it), 1920, 1080)
    #recordStats(active_var + '(part den 2d)')

    DeleteAllPlots()
