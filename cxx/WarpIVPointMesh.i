%define MDOC
"operations that map a point set to a uniform mesh

PyArrayObject *meshIds(
    PyArrayObject *ax, PyArrayObject *ay, PyArrayObject *az,
    double x0, double y0, double z0,
    double dx, double dy, double dz,
    int nxc, int nyc, int nzc)

    compute the set of ids that maps the points onto
    a uniform Cartesian mesh

    inputs:

        ax,ay,az -- point coordinate arrays
        x0,y0,z0 -- mesh origin
        dx,dy,dz -- mesh spacing
        ncx,ncy,ncz -- number of cells in each direction

    returns:
         the mesh id of each point.


PyArrayObject *meshDensity(
    PyArrayObject *aids, mesh_id_t ncells, int type)

    compute the density of a point set on a uniform
    Cartesian mesh

    inputs:

        aids -- a set of mesh ids precomputed with meshIds
        ncells -- number of cells in the mesh
        type -- type to use when creating the output

    returns:
        the density of the point set on the mesh


PyArrayObject *meshAverage(
    PyArrayObject *aar, PyArrayObject *aids, PyArrayObject *aden)

    move a scalar array from a point set onto a uniform
    Cartesian mesh using an avaerage

    inputs:

        aar -- the array to map to the mesh
        aids -- a set of mesh ids precomputed with meshIds
        aden -- precomputed density of points on the mesh

    returns:

        the array mapped to the mesh using the average


PyArrayObject *meshMin(
    PyArrayObject *aar, PyArrayObject *aids, mesh_id_t ncells)

    move a scalar array from a point set onto a uniform
    Cartesian mesh using a min

    inputs:

        aar -- the array to map to the mesh
        ncells -- number of cells in the mesh

    returns:

        the array mapped to the mesh using the min


PyArrayObject *meshMax(
    PyArrayObject *aar, PyArrayObject *aids, mesh_id_t ncells)

    move a scalar array from a point set onto a uniform
    Cartesian mesh using a max

    inputs:

        aar -- the array to map to the mesh
        ncells -- number of cells in the mesh

    returns:

        the array mapped to the mesh using the max

"
%enddef

%module (docstring=MDOC) WarpIVPointMesh

%{
#define SWIG_FILE_WITH_INIT
#include <stdio.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#define PY_ARRAY_UNIQUE_SYMBOL  PyArray_API_pointMesh
#include <numpy/arrayobject.h>
#include "WarpIVGlue.h"
#include "WarpIVPointMesh.h"
%}

%init %{
import_array();
%}

%include "WarpIVTypemaps.i"
%include "WarpIVMeshId.h"

%inline %{
// **************************************************************************
PyArrayObject *xzMeshIds(
    PyArrayObject *ax, PyArrayObject *az,
    double x0, double z0, double dx, double dz,
    mesh_id_t nxc, mesh_id_t nzc)
{
    // allocate the ids
    mesh_id_t npts = getSize(ax);
    PyArrayObject *aids = allocatePyArray<mesh_id_t>(npts);
    mesh_id_t *ids = static_cast<mesh_id_t*>(PyArray_DATA(aids));

    // dispatch based on point type
    switch (getType(ax))
    {
        numpyDispatch(

            CPP_TT::Type *x = NULL;
            CPP_TT::Type *z = NULL;

            if (getCPointer(ax, x) || getCPointer(az, z))
            {
                PyErr_Format(PyExc_ValueError,
                    "failed to get pointer to coordinates");
                return NULL;
            }

            meshIds<CPP_TT::Type>(x,z,npts,x0,z0,dx,dz,nxc,nzc,ids);
        )
        default:
            PyErr_Format(PyExc_ValueError, "failed dispatch unsupported type");
            return NULL;
    }

    return aids;
}

// **************************************************************************
PyArrayObject *meshIds(
    PyArrayObject *ax, PyArrayObject *ay, PyArrayObject *az,
    double x0, double y0, double z0,
    double dx, double dy, double dz,
    mesh_id_t nxc, mesh_id_t nyc, mesh_id_t nzc)
{
    // allocate the ids
    mesh_id_t npts = getSize(ax);
    PyArrayObject *aids = allocatePyArray<mesh_id_t>(npts);
    mesh_id_t *ids = static_cast<mesh_id_t*>(PyArray_DATA(aids));

    // dispatch based on point type
    switch (getType(ax))
    {
        numpyDispatch(

            CPP_TT::Type *x = NULL;
            CPP_TT::Type *y = NULL;
            CPP_TT::Type *z = NULL;

            if (getCPointer(ax, x) || getCPointer(ay, y) || getCPointer(az, z))
            {
                PyErr_Format(PyExc_ValueError,
                    "failed to get pointer to coordinates");
                return NULL;
            }

            meshIds<CPP_TT::Type>(x,y,z,npts,x0,y0,z0,dx,dy,dz,nxc,nyc,nzc,ids);
        )
        default:
            PyErr_Format(PyExc_ValueError, "failed dispatch unsupported type");
            return NULL;
    }

    return aids;
}

// **************************************************************************
PyArrayObject *meshDensity(PyArrayObject *aids, mesh_id_t ncells, int type)
{
    // get a pointer to the id array
    mesh_id_t *ids = NULL, nids = 0;
    if (getCPointer(aids, ids, nids))
    {
        PyErr_Format(PyExc_ValueError,
            "failed to get a pointer to the ids");
        return NULL;
    }

    // dispatch based on output type
    PyArrayObject *aden = NULL;
    switch (type)
    {
        numpyDispatch(

            // allocate the density array
            aden = allocatePyArray<CPP_TT::Type>(ncells);
            CPP_TT::Type *den = static_cast<CPP_TT::Type*>(PyArray_DATA(aden));

            meshDensity(ids, nids, ncells, den);
        )
        default:
            PyErr_Format(PyExc_ValueError, "failed dispatch unsupported type");
            return NULL;
    }

    return aden;
}

// **************************************************************************
PyArrayObject *meshAverage(
    PyArrayObject *aar, PyArrayObject *aids, PyArrayObject *aden)
{
    // get a pointer to the id array
    mesh_id_t *ids = NULL, nids = 0;
    if (getCPointer(aids, ids, nids))
    {
        PyErr_Format(PyExc_ValueError,
            "failed to get a pointer to the ids");
        return NULL;
    }

    // dispatch based on output type.
    PyArrayObject *aavg = NULL;
    switch (getType(aar))
    {
        numpyDispatch(

            CPP_TT::Type *ar = NULL;
            CPP_TT::Type *den = NULL;
            mesh_id_t ncells = 0;

            if (getCPointer(aar, ar) || getCPointer(aden, den, ncells))
            {
                PyErr_Format(PyExc_ValueError,
                    "failed to get pointer to input array");
                return NULL;
            }

            // allocate the output array
            aavg = allocatePyArray<CPP_TT::Type>(ncells);
            CPP_TT::Type *avg = static_cast<CPP_TT::Type*>(PyArray_DATA(aavg));

            meshAverage(ar, ids, nids, ncells, den, avg);
        )
        default:
            PyErr_Format(PyExc_ValueError, "failed dispatch unsupported type");
            return NULL;
    }

    return aavg;
}

// **************************************************************************
PyArrayObject *meshMin(
    PyArrayObject *aar, PyArrayObject *aids, mesh_id_t ncells)
{
    // get a pointer to the id array
    mesh_id_t *ids = NULL, nids = 0;
    if (getCPointer(aids, ids, nids))
    {
        PyErr_Format(PyExc_ValueError,
            "failed to get a pointer to the ids");
        return NULL;
    }

    // dispatch based on output type
    PyArrayObject *amn = NULL;
    switch (getType(aar))
    {
        numpyDispatch(

            CPP_TT::Type *ar = NULL;
            if (getCPointer(aar, ar))
            {
                PyErr_Format(PyExc_ValueError,
                    "failed to get pointer to input array");
                return NULL;
            }

            // allocate the output array
            amn = allocatePyArray<CPP_TT::Type>(ncells);
            CPP_TT::Type *mn = static_cast<CPP_TT::Type*>(PyArray_DATA(amn));

            meshMin(ar,ids,nids,ncells,mn);
        )
        default:
            PyErr_Format(PyExc_ValueError, "failed dispatch unsupported type");
            return NULL;
    }

    return amn;
}

// **************************************************************************
PyArrayObject *meshMax(
    PyArrayObject *aar, PyArrayObject *aids, mesh_id_t ncells)
{
    // get a pointer to the id array
    mesh_id_t *ids = NULL, nids = 0;
    if (getCPointer(aids, ids, nids))
    {
        PyErr_Format(PyExc_ValueError,
            "failed to get a pointer to the ids");
        return NULL;
    }

    // dispatch based on output type
    PyArrayObject *amx = NULL;
    switch (getType(aar))
    {
        numpyDispatch(

            CPP_TT::Type *ar = NULL;
            if (getCPointer(aar, ar))
            {
                PyErr_Format(PyExc_ValueError,
                    "failed to get pointer to input array");
                return NULL;
            }

            // allocate the output array
            amx = allocatePyArray<CPP_TT::Type>(ncells);
            CPP_TT::Type *mx = static_cast<CPP_TT::Type*>(PyArray_DATA(amx));

            meshMax(ar,ids,nids,ncells,mx);
        )
        default:
            PyErr_Format(PyExc_ValueError, "failed dispatch unsupported type");
            return NULL;
    }

   return amx;
}
%}
