import numpy as np
import simV2
from WarpIVUtil import pError, pDebug

#-----------------------------------------------------------------------------
def getMetaData(source):
    """
    Callback function used to provide visit with metadata
    """
    expname = lambda x : x[0]
    expstr = lambda x : x[1]
    exptype = lambda x : x[2]
    valid = lambda x : x != simV2.VISIT_INVALID_HANDLE
    if __debug__: pDebug('getMetaData')
    try:

        # VisIt assumes that each process has some data
        # so you must have at least as many domains as
        # processes. you just return invalid handle in
        # getMesh/getVar on those processes that do not

        simmd = simV2.VisIt_SimulationMetaData_alloc()
        if not valid(simmd):
            pError('VisIt_SimulationMetaData_alloc failed')
            return simV2.VISIT_INVALID_HANDLE

        # time
        simV2.VisIt_SimulationMetaData_setCycleTime(simmd,
              source.GetTimeStep(), source.GetTime())

        # particles
        # per species data
        pSource = source.GetParticleSource()
        pVarNames = pSource.GetArrayNames()
        pMeshNames = pSource.GetMeshNames()
        if len(pMeshNames) < 1:
            pError('No particle mesh names were found')
        for pMeshName in pMeshNames:
            meshmd = simV2.VisIt_MeshMetaData_alloc()
            simV2.VisIt_MeshMetaData_setName(meshmd, pMeshName)
            simV2.VisIt_MeshMetaData_setMeshType(meshmd, simV2.VISIT_MESHTYPE_POINT)
            simV2.VisIt_MeshMetaData_setTopologicalDimension(meshmd, 0)
            simV2.VisIt_MeshMetaData_setSpatialDimension(meshmd, 3)
            simV2.VisIt_MeshMetaData_setNumDomains(meshmd, pSource.GetNumberOfDomains())
            simV2.VisIt_MeshMetaData_setDomainTitle(meshmd, 'Domains')
            simV2.VisIt_MeshMetaData_setDomainPieceName(meshmd, 'domain')
            simV2.VisIt_MeshMetaData_setNumGroups(meshmd, 0)
            simV2.VisIt_MeshMetaData_setXUnits(meshmd, 'm')
            simV2.VisIt_MeshMetaData_setYUnits(meshmd, 'm')
            simV2.VisIt_MeshMetaData_setZUnits(meshmd, 'm')
            simV2.VisIt_MeshMetaData_setXLabel(meshmd, 'x')
            simV2.VisIt_MeshMetaData_setYLabel(meshmd, 'y')
            simV2.VisIt_MeshMetaData_setZLabel(meshmd, 'z')
            simV2.VisIt_SimulationMetaData_addMesh(simmd, meshmd)
            for var in pVarNames:
                vmd = simV2.VisIt_VariableMetaData_alloc()
                if not valid(vmd):
                    pError('VisIt_VariableMetaData_alloc failed')
                    return simV2.VISIT_INVALID_HANDLE
                varname = '%s%s%s'%(pMeshName,'/',var)
                simV2.VisIt_VariableMetaData_setName(vmd, varname)
                simV2.VisIt_VariableMetaData_setMeshName(vmd, pMeshName)
                simV2.VisIt_VariableMetaData_setType(vmd, simV2.VISIT_VARTYPE_SCALAR)
                simV2.VisIt_VariableMetaData_setCentering(vmd, simV2.VISIT_VARCENTERING_NODE)
                simV2.VisIt_SimulationMetaData_addVariable(simmd, vmd)

                if len(var) == 2 and var[-1] == 'x':
                    # vector expressions
                    exp = ('{0}/{1}','{{<{0}/{1}x>, <{0}/{1}y>, <{0}/{1}z>}}',simV2.VISIT_VARTYPE_VECTOR)
                    expmd = simV2.VisIt_ExpressionMetaData_alloc()
                    if not valid(expmd):
                        pError('VisIt_ExpressionMetaData_alloc failed')
                        return simV2.VISIT_INVALID_HANDLE
                    simV2.VisIt_ExpressionMetaData_setName(expmd, expname(exp).format(pMeshName, var[0]))
                    simV2.VisIt_ExpressionMetaData_setDefinition(expmd, expstr(exp).format(pMeshName, var[0]))
                    simV2.VisIt_ExpressionMetaData_setType(expmd, exptype(exp))
                    simV2.VisIt_SimulationMetaData_addExpression(simmd, expmd)

        # grid
        mSource = source.GetMeshSource()
        meshmd = simV2.VisIt_MeshMetaData_alloc()
        if not valid(meshmd):
            pError('VisIt_MeshMetaData_alloc failed')
            return simV2.VISIT_INVALID_HANDLE
        simV2.VisIt_MeshMetaData_setName(meshmd, 'grid')
        simV2.VisIt_MeshMetaData_setMeshType(meshmd, simV2.VISIT_MESHTYPE_RECTILINEAR)
        simV2.VisIt_MeshMetaData_setTopologicalDimension(meshmd, 3)
        simV2.VisIt_MeshMetaData_setSpatialDimension(meshmd, 3)
        simV2.VisIt_MeshMetaData_setNumDomains(meshmd, mSource.GetNumberOfDomains())
        simV2.VisIt_MeshMetaData_setDomainTitle(meshmd, 'Domains')
        simV2.VisIt_MeshMetaData_setDomainPieceName(meshmd, 'domain')
        simV2.VisIt_MeshMetaData_setNumGroups(meshmd, 0)
        simV2.VisIt_MeshMetaData_setXUnits(meshmd, 'm')
        simV2.VisIt_MeshMetaData_setYUnits(meshmd, 'm')
        simV2.VisIt_MeshMetaData_setZUnits(meshmd, 'm')
        simV2.VisIt_MeshMetaData_setXLabel(meshmd, 'x')
        simV2.VisIt_MeshMetaData_setYLabel(meshmd, 'y')
        simV2.VisIt_MeshMetaData_setZLabel(meshmd, 'z')
        simV2.VisIt_SimulationMetaData_addMesh(simmd, meshmd)

        # grid data
        varnames = mSource.GetArrayNames()
        for varname in varnames:
            vmd = simV2.VisIt_VariableMetaData_alloc()
            if not valid(vmd):
                pError('VisIt_VariableMetaData_alloc failed')
                return simV2.VISIT_INVALID_HANDLE
            simV2.VisIt_VariableMetaData_setName(vmd, varname)
            simV2.VisIt_VariableMetaData_setMeshName(vmd, 'grid')
            simV2.VisIt_VariableMetaData_setType(vmd, simV2.VISIT_VARTYPE_SCALAR)
            # variables are converted from cell, face or edge centering
            # to node centering when needed. note: for non node centered data
            # warp returns it on a node center size mesh, which because
            # of guard cells has valid values everywhere, so you are always
            # safe to treat it as a node centered mesh, but there may be
            # some subtle differences when applying vector analysis if
            # the conversion isn't done.
            simV2.VisIt_VariableMetaData_setCentering(vmd, simV2.VISIT_VARCENTERING_NODE)
            #TODO
            #simV2.VisIt_VariableMetaData_setUnits(vmd, var_props['units'])
            simV2.VisIt_SimulationMetaData_addVariable(simmd, vmd)

            if varname[-1] == 'x':
                # vector expressions
                exp = ('{0}','{{<{0}x>, <{0}y>, <{0}z>}}',simV2.VISIT_VARTYPE_VECTOR)
                expmd = simV2.VisIt_ExpressionMetaData_alloc()
                if not valid(expmd):
                    pError('VisIt_ExpressionMetaData_alloc failed')
                    return simV2.VISIT_INVALID_HANDLE
                simV2.VisIt_ExpressionMetaData_setName(expmd, expname(exp).format(varname[0:-1]))
                simV2.VisIt_ExpressionMetaData_setDefinition(expmd, expstr(exp).format(varname[0:-1]))
                simV2.VisIt_ExpressionMetaData_setType(expmd, exptype(exp))
                simV2.VisIt_SimulationMetaData_addExpression(simmd, expmd)

        # commands
        for cmd in ['step','run','continue','pause','end','disconnect']:
            cmdmd = simV2.VisIt_CommandMetaData_alloc()
            if not valid(cmdmd):
                pError('VisIt_CommandMetaData_alloc failed')
                return simV2.VISIT_INVALID_HANDLE
            simV2.VisIt_CommandMetaData_setName(cmdmd, cmd)
            simV2.VisIt_SimulationMetaData_addGenericCommand(simmd, cmdmd)

        return simmd

    except Exception as inst:
        pError('Warp failed to create metadata\n%s\n'%(str(inst)))
        return simV2.VISIT_INVALID_HANDLE

#-----------------------------------------------------------------------------
def getMesh(domain, name, source):
    """
    Callback function used to send mesh data (e.g., particles) to VisIt.
    """
    if __debug__: pDebug('getMesh %i %s'%(domain, name))
    # VisIt assumes that each process has some data
    # so you must have at least as many domains as
    # processes. you just return invalid handle in
    # getMesh/getVar on those processes that do not
    valid = lambda x : x != simV2.VISIT_INVALID_HANDLE
    try:
        # particle mesh
        pSource = source.GetParticleSource()
        pMeshNames = pSource.GetMeshNames()
        if pMeshNames.count(name) > 0:

            coords = pSource.GetCoordinates(name)
            if coords is None:
                pError('Failed to get particle mesh coordinates for %s'%(name))
                return simV2.VISIT_INVALID_HANDLE

            xvd = passParticleData(coords[0])
            yvd = passParticleData(coords[1])
            zvd = passParticleData(coords[2])

            if (not (valid(xvd) and valid(yvd) and valid(zvd))):
                if __debug__: pDebug('failed to pass particle locations')
                return simV2.VISIT_INVALID_HANDLE

            mesh = simV2.VisIt_PointMesh_alloc()
            if not valid(mesh):
                pError('VisIt_PointMesh_alloc failed')
                return simV2.VISIT_INVALID_HANDLE

            simV2.VisIt_PointMesh_setCoordsXYZ(mesh, xvd, yvd, zvd)
            return mesh

        # uniform mesh
        if name == 'grid':
            mSource = source.GetMeshSource()
            coords = mSource.GetCoordinates()

            xvd = passGridData(coords[0])
            yvd = passGridData(coords[1])
            zvd = passGridData(coords[2])

            if (not (valid(xvd) and valid(yvd) and valid(zvd))):
                pError('failed to pass mesh coords')
                return simV2.VISIT_INVALID_HANDLE

            mesh = simV2.VisIt_RectilinearMesh_alloc()
            if not valid(mesh):
                pError('VisIt_RectilinearMesh_alloc failed')
                return simV2.VISIT_INVALID_HANDLE

            simV2.VisIt_RectilinearMesh_setCoordsXYZ(mesh, xvd, yvd, zvd)
            return mesh

        # the meshname requested is not one we know about
        pError('Unrecognized mesh name %s'%(name))
        return simV2.VISIT_INVALID_HANDLE

    except Exception as inst:
        pError('Warp failed to produce mesh %s for domain %d\n%s\n'%( \
            name, domain, str(inst)))
        return simV2.VISIT_INVALID_HANDLE

#-----------------------------------------------------------------------------
def getVar(domain, varid, source):
    """
    Callback function used to send variable data (e.g., vx) to VisIt.
    """
    if __debug__: pDebug('getVar %i %s'%(domain, varid))
    # VisIt assumes that each process has some data
    # so you must have at least as many domains as
    # processes. you just return invalid handle in
    # getMesh/getVar on those processes that do not
    valid = lambda x : x != simV2.VISIT_INVALID_HANDLE
    try:
        # parse the request
        meshName, varName, srcMeshName, derivedQty, opName = source.DecodeDataSpec(varid)

        # particle data
        pSource = source.GetParticleSource()
        pMeshNames = pSource.GetMeshNames()
        if pMeshNames.count(meshName) > 0:
            varout = pSource.GetArray(meshName, varName)
            if varout is not None:
                return passParticleData(varout)

        # mesh data
        elif meshName == 'grid':
            mSource = source.GetMeshSource()
            varout = mSource.GetArray(meshName, varName, srcMeshName, derivedQty, opName)
            if varout is not None:
                return passGridData(varout)

        # invalid
        pError('Unrecognized variable requested %s'%(varid))
        return simV2.VISIT_INVALID_HANDLE

    # the simulation crashed when we asked for that variable
    except Exception as inst:
        pError('Warp failed to produce %s\n%s\n'%(varName,str(inst)))
        return simV2.VISIT_INVALID_HANDLE


#-----------------------------------------------------------------------------
def getDomains(name, source):
    """
    Callback function used to get the list of domains handled by this process
    """
    valid = lambda x : x != simV2.VISIT_INVALID_HANDLE

    # VisIt assumes that each process has some data
    # so you must have at least as many domains as
    # processes. you just return invalid handle in
    # getMesh/getVar on those processes that do not
    if __debug__: pDebug('getDomains %s'%(name))

    mSource = source.GetMeshSource()
    nDoms = mSource.GetNumberOfDomains()
    local = mSource.GetDomainIds()

    vd = simV2.VisIt_VariableData_alloc()
    if not valid(vd):
        pError('VisIt_VariableData_alloc failed')
        return None
    simV2.VisIt_VariableData_setDataI(vd, simV2.VISIT_OWNER_COPY, 1, 1, local)

    doms = simV2.VisIt_DomainList_alloc()
    if not valid(doms):
        pError('VisIt_DomainList_alloc failed')
        return None
    simV2.VisIt_DomainList_setDomains(doms, nDoms, vd)

    return doms

#-----------------------------------------------------------------------------
def passGridData(data, owner=simV2.VISIT_OWNER_VISIT_EX):
    """Helper for passing data to VisIt"""
    # TODO -- how come we have to transpose it?
    # IIRC warp is setting the c-order flag on the numpy array
    # taking the transpose puts it in fortran order wrt numpy
    # iterators. this is something that we need to work out with
    # warp guys because in visit we will need to handle other sims
    # and can't make assumptions about the ordering, and thus need
    # to rely on array flags being set correctly.
    return passData(data.transpose(), owner)

#-----------------------------------------------------------------------------
def passParticleData(data, owner=simV2.VISIT_OWNER_VISIT_EX):
    """Helper for passing data to VisIt"""
    return passData(data, owner)

#-----------------------------------------------------------------------------
def passData(data, owner=simV2.VISIT_OWNER_VISIT_EX):
    """Helper for passing data to VisIt"""
    valid = lambda x : x != simV2.VISIT_INVALID_HANDLE

    if not data.size:
        # not always an error, some processes
        # wont have data.
        #if __debug__: pDebug('zero size for %s'%(str(data)))
        return simV2.VISIT_INVALID_HANDLE

    vd = simV2.VisIt_VariableData_alloc()
    if not valid(vd):
        pError('VisIt_VariableData_alloc failed')
        return simV2.VISIT_INVALID_HANDLE

    ierr = simV2.VISIT_ERROR
    if (data.dtype == np.float64):
        ierr = simV2.VisIt_VariableData_setDataD(vd, owner, 1, data.size, data)
    elif (data.dtype == np.float32):
        ierr = simV2.VisIt_VariableData_setDataF(vd, owner, 1, data.size, data)
    elif (data.dtype == np.int32):
        ierr = simV2.VisIt_VariableData_setDataI(vd, owner, 1, data.size, data)
    elif (data.dtype == np.byte):
        ierr = simV2.VisIt_VariableData_setDataC(vd, owner, 1, data.size, data)

    if (ierr == simV2.VISIT_ERROR):
        pError('VisIt_VariableData_setData failed')
        return simV2.VISIT_INVALID_HANDLE

    return vd
