#ifndef WarpIVYeeCellWrap_h
#define WarpIVYeeCellWrap_h

// helper for wrapping 2d conversion calls. they all have the
// same inputs and outputs.
#define d2_conversion_wrapper(_py_name, _c_name)                                    \
PyArrayObject *_py_name(                                                            \
    PyArrayObject *ain,                                                             \
    mesh_id_t nxi, mesh_id_t nzi,                                                   \
    mesh_id_t nxo, mesh_id_t nzo,                                                   \
    mesh_id_t ngx, mesh_id_t ngz);

// helper for wrapping 3d conversion calls. they all have the
// same inputs and outputs.
#define d3_conversion_wrapper(_py_name, _c_name)                                    \
PyArrayObject *_py_name(                                                            \
    PyArrayObject *ain,                                                             \
    mesh_id_t nxi, mesh_id_t nyi, mesh_id_t nzi,                                    \
    mesh_id_t nxo, mesh_id_t nyo, mesh_id_t nzo,                                    \
    mesh_id_t ngx, mesh_id_t ngy, mesh_id_t ngz);

d2_conversion_wrapper(cpy2, cpy)
d2_conversion_wrapper(xAvg2, xAvg)
d2_conversion_wrapper(zAvg2, zAvg)
d2_conversion_wrapper(xzAvg2, xzAvg)

d3_conversion_wrapper(cpy3, cpy)
d3_conversion_wrapper(xAvg3, xAvg)
d3_conversion_wrapper(yAvg3, yAvg)
d3_conversion_wrapper(zAvg3, zAvg)
d3_conversion_wrapper(xyAvg3, xyAvg)
d3_conversion_wrapper(xzAvg3, xzAvg)
d3_conversion_wrapper(yzAvg3, yzAvg)
d3_conversion_wrapper(xyzAvg3, xyzAvg)

#endif
