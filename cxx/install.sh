#!/bin/bash

if [[ -z "$1" ]]
then
    echo "usage: install /dest/path"
    exit
fi

./clean.sh
./swig.sh
python setup.py build_ext --inplace
cp *.py *.so $1
