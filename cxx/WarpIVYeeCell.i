%define MDOC
"operations for moving between a Yee cell and nodal meshes

there are methods for 2d xz meshes and 3d meshes. they all
follow the same pattern:

PyArrayObject *method2d(
    PyArrayObject *ain,
    mesh_id_t nxi, mesh_id_t nzi,
    mesh_id_t nxo, mesh_id_t nzo,
    mesh_id_t ngx, mesh_id_t ngz)

    convert from input mesh to output mesh

    inputs:
        ain -- input array
        nxi,nzi -- input array dims
        nxo,nzo -- output array dims
        ngx,ngz -- input array ghost cells

    returns:
        output array


PyArrayObject *method3d(
    PyArrayObject *ain,
    mesh_id_t nxi, mesh_id_t nyi, mesh_id_t nzi,
    mesh_id_t nxo, mesh_id_t nyo, mesh_id_t nzo,
    mesh_id_t ngx, mesh_id_t ngy, mesh_id_t ngz)

    convert from input mesh to output mesh

    inputs:
        ain -- input array
        nxi,nyi,nzi -- input array dims
        nxo,nyo,nzo -- output array dims
        ngx,ngy,ngz -- input array ghost cells

    returns:
        output array
"
%enddef

%module (docstring=MDOC) WarpIVYeeCell

%{
#define SWIG_FILE_WITH_INIT
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#define PY_ARRAY_UNIQUE_SYMBOL  PyArray_API_yeeCell
#include <numpy/arrayobject.h>
#include <stdio.h>
#include "WarpIVGlue.h"
#include "WarpIVYeeCell.h"
#include "WarpIVYeeCellWrap.h"
%}

%init %{
import_array();
%}

%include "WarpIVTypemaps.i"
%include "WarpIVMeshId.h"
%include "WarpIVYeeCellWrap.h"

%inline %{
%}
