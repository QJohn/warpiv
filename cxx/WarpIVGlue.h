#ifndef WarpIVGlue_h
#define WarpIVGlue_h

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>
#include <stdlib.h>

template<typename T> class CppToNumpy {};
template<> class CppToNumpy<float> { public: enum { Type = NPY_FLOAT }; };
template<> class CppToNumpy<double> { public: enum { Type = NPY_DOUBLE }; };
template<> class CppToNumpy<int> { public: enum { Type = NPY_INT }; };
template<> class CppToNumpy<unsigned int> { public: enum { Type = NPY_UINT }; };

template<int C> class NumpyToCpp {};
template<> class NumpyToCpp<NPY_FLOAT> { public: typedef float Type; };
template<> class NumpyToCpp<NPY_DOUBLE> { public: typedef double Type; };
template<> class NumpyToCpp<NPY_INT> { public: typedef int Type; };
template<> class NumpyToCpp<NPY_UINT> { public: typedef unsigned int Type; };

#define pyObjectCast(_ptr) \
    reinterpret_cast<PyObject*>(_ptr)

#define numpyDispatchCase(_type_enum, _code)    \
    case _type_enum:                            \
        {                                       \
        typedef NumpyToCpp<_type_enum> CPP_TT;  \
        typedef CppToNumpy<CPP_TT> NUMPY_TT;    \
        _code                                   \
        }                                       \
        break;

#define numpyDispatch(_code)                \
    numpyDispatchCase(NPY_FLOAT, _code)     \
    numpyDispatchCase(NPY_DOUBLE, _code)    \
    numpyDispatchCase(NPY_INT, _code)       \
    numpyDispatchCase(NPY_UINT, _code)

/******************************************************************************/
inline
int pyArrayObjectCast(void *obj, PyArrayObject *&ptr)
{
    if (!PyArray_Check(reinterpret_cast<PyObject*>(obj)))
    {
        // not a numpy array
        return -1;
    }
    ptr = reinterpret_cast<PyArrayObject*>(obj);
    return 0;
}

/******************************************************************************/
inline
int getSize(PyArrayObject *arr)
{ return PyArray_SIZE(arr); }

/******************************************************************************/
template <typename ptr_t>
int getCPointer(PyArrayObject *arr, ptr_t *&ptr)
{
    if (PyArray_TYPE(arr) != CppToNumpy<ptr_t>::Type)
    {
        // internal datatype doesn't match expected type
        return -1;
    }

    if (!(PyArray_IS_C_CONTIGUOUS(arr) || PyArray_IS_F_CONTIGUOUS(arr)))
    {
        // the array is not contiguous
        return -2;
    }

    // success, safe to use the pointer
    ptr = static_cast<ptr_t*>(PyArray_DATA(arr));
    return 0;
}

/******************************************************************************/
template <typename ptr_t, typename sz_t>
int getCPointer(PyArrayObject *arr, ptr_t *&ptr, sz_t &sz)
{
    if (PyArray_TYPE(arr) != CppToNumpy<ptr_t>::Type)
    {
        // internal datatype doesn't match expected type
        return -1;
    }

    if (!(PyArray_IS_C_CONTIGUOUS(arr) || PyArray_IS_F_CONTIGUOUS(arr)))
    {
        // the array is not contiguous
        return -2;
    }

    // success, safe to use the pointer
    sz = PyArray_SIZE(arr);
    ptr = static_cast<ptr_t*>(PyArray_DATA(arr));
    return 0;
}

/******************************************************************************/
inline
int getType(PyArrayObject *arr)
{ return PyArray_TYPE(arr); }


#ifndef aligned_alloc
#define aligned_alloc(_a, _n) \
    malloc(_n)
#endif

// ****************************************************************************
template<typename T>
PyArrayObject *allocatePyArray(npy_intp n)
{
    T *mem = static_cast<T*>(aligned_alloc(64, n*sizeof(T)));
    if (!mem)
    {
        fprintf(stderr, "failed to allocate %lu bytes", n*sizeof(T));
        abort();
        return NULL;
    }

    // pass into a numpy object
    npy_intp dim[1] = {n};
    PyArrayObject *arr = reinterpret_cast<PyArrayObject*>(
        PyArray_SimpleNewFromData(1, dim, CppToNumpy<T>::Type, mem));
    PyArray_ENABLEFLAGS(arr, NPY_ARRAY_OWNDATA);

    return arr;
}

#endif
