import numpy
import warp
import parallel
import inspect
import types
import sys
from WarpIVUtil import pError, pDebug, pStatus

#############################################################################
class SpecType(object):
    #------------------------------------------------------------------------
    def __init__(self, name):
        self.name = name

#############################################################################
class WarpIVMergedSpecies(object):
    """
    Interface to Warp Species class that supports merging of
    a number of species into a meta species containing their
    agregate data. Construct it with a list of species and
    a name for the new meta species
    """
    #------------------------------------------------------------------------
    def __init__(self, species, name, stype=None, norm=False):
        """
        Initalize the WarpIVMergedSpecies object:

        Parameters:
            species : list of species to merge
            name : name of the new meta species containing the merged species
            rescale : will normalize the output of each species onto a unique range

        Attributes:
          Public interface to Warp's Species:
            name : Name for the filtered species, defined by filterName and species.name
            type : Type of the species defined by species.type.
        """
        self._Species = species
        self._Norm = norm
        self.name = name
        self.type = species[0].type if stype is None else SpecType(stype)
        return

    #------------------------------------------------------------------------
    def Merge(self, getter, **kwargs):
        """
        Use the passed in getter to concatenate data from all
        the managed species into a single array which is returned

        Parameters:
            getter : name of the get function to use
            kwargs : args to pass to the getters
        """
        data = numpy.array([])
        ofs = 0.0
        for spec in self._Species:
            specGetter = getattr(spec, getter)
            if (not self._Norm) \
                or (getter == 'getx') or (getter == 'gety') or (getter == 'getz'):
                data = numpy.append(data, specGetter(**kwargs))
            else:
                data = numpy.append(data, self.Normalize(specGetter(**kwargs), ofs))
                ofs += 2.0
        return data

    #------------------------------------------------------------------------
    def Normalize(self, data, ofs):
        """
        Map the data onto [0-1], and add the offset
        """
        mn = parallel.parallelmin(numpy.min(data) if data.size else sys.float_info.max)
        mx = parallel.parallelmax(numpy.max(data) if data.size else -sys.float_info.max)
        rng = mx - mn
        if abs(rng) < 1e-10:
            pError('Failed to normalize. degenerate range')
            return data
        newdata = (data - mn)/rng + ofs
        return newdata

    #------------------------------------------------------------------------
    def getpid(self,**kwargs): return self.Merge('getpid',**kwargs)
    def getn(self,**kwargs): return self.Merge('getn',**kwargs)
    def getx(self,**kwargs): return self.Merge('getx',**kwargs)
    def gety(self,**kwargs): return self.Merge('gety',**kwargs)
    def getz(self,**kwargs): return self.Merge('getz',**kwargs)
    def getw(self,**kwargs): return self.Merge('getw',**kwargs)
    def getr(self,**kwargs): return self.Merge('getr',**kwargs)
    def gettheta(self,**kwargs): return self.Merge('gettheta',**kwargs)
    def getvtheta(self,**kwargs): return self.Merge('getvtheta',**kwargs)
    def getetheta(self,**kwargs): return self.Merge('getetheta',**kwargs)
    def getbtheta(self,**kwargs): return self.Merge('getbtheta',**kwargs)
    def getvx(self,**kwargs): return self.Merge('getvx',**kwargs)
    def getvy(self,**kwargs): return self.Merge('getvy',**kwargs)
    def getvz(self,**kwargs): return self.Merge('getvz',**kwargs)
    def getvr(self,**kwargs): return self.Merge('getvr',**kwargs)
    def getux(self,**kwargs): return self.Merge('getux',**kwargs)
    def getuy(self,**kwargs): return self.Merge('getuy',**kwargs)
    def getuz(self,**kwargs): return self.Merge('getuz',**kwargs)
    def getex(self,**kwargs): return self.Merge('getex',**kwargs)
    def getey(self,**kwargs): return self.Merge('getey',**kwargs)
    def getez(self,**kwargs): return self.Merge('getez',**kwargs)
    def geter(self,**kwargs): return self.Merge('geter',**kwargs)
    def getbx(self,**kwargs): return self.Merge('getbx',**kwargs)
    def getby(self,**kwargs): return self.Merge('getby',**kwargs)
    def getbz(self,**kwargs): return self.Merge('getbz',**kwargs)
    def getbr(self,**kwargs): return self.Merge('getbr',**kwargs)
    def getxp(self,**kwargs): return self.Merge('getxp',**kwargs)
    def getyp(self,**kwargs): return self.Merge('getyp',**kwargs)
    def getzp(self,**kwargs): return self.Merge('getzp',**kwargs)
    def getrp(self,**kwargs): return self.Merge('getrp',**kwargs)
    def gettp(self,**kwargs): return self.Merge('gettp',**kwargs)
    def getgaminv(self,**kwargs): return self.Merge('getgaminv',**kwargs)
    def getweights(self,**kwargs): return self.Merge('getweights',**kwargs)
    def getke(self,**kwargs): return self.Merge('getke',**kwargs)
    def getrank(self,**kwargs): return self.Merge('getrank',**kwargs)
