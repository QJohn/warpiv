# Script showing all protons in gray and the beam particles in color

# counter for cycling various script params
try:
    protonbeam_it
except NameError:
    protonbeam_it = 0
else:
    protonbeam_it += 1

# this ensures a consistent axes
AddPlot("Mesh", "grid", 1, 0)
DrawPlots()
HideActivePlots()

proton_vars = ["Proton-0/ke", "Proton-protonbeam/ke"]
for pi, pvar in enumerate(proton_vars):

    plot_all_protons = (pi==0)

    # Add the plot with the protons
    AddPlot("Pseudocolor", pvar, 1, 0)
    PseudocolorAtts = PseudocolorAttributes()
    PseudocolorAtts.scaling = PseudocolorAtts.Linear  # Linear, Log, Skew
    PseudocolorAtts.skewFactor = 1
    PseudocolorAtts.limitsMode = PseudocolorAtts.OriginalData  # OriginalData, CurrentPlot
    PseudocolorAtts.minFlag = 1 if plot_all_protons else 0
    PseudocolorAtts.min = -1
    PseudocolorAtts.maxFlag = 0
    PseudocolorAtts.max = 1
    PseudocolorAtts.centering = PseudocolorAtts.Natural  # Natural, Nodal, Zonal
    PseudocolorAtts.colorTableName = 'xray' if plot_all_protons else 'hot'
    PseudocolorAtts.invertColorTable = 0
    PseudocolorAtts.opacityType = PseudocolorAtts.Constant  # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
    PseudocolorAtts.opacityVariable = ""
    PseudocolorAtts.opacity = 1.0
    PseudocolorAtts.opacityVarMin = 0
    PseudocolorAtts.opacityVarMax = 1
    PseudocolorAtts.opacityVarMinFlag = 0
    PseudocolorAtts.opacityVarMaxFlag = 0
    PseudocolorAtts.pointSize = 0.05
    PseudocolorAtts.pointType = PseudocolorAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
    PseudocolorAtts.pointSizeVarEnabled = 0
    PseudocolorAtts.pointSizeVar = "default"
    PseudocolorAtts.pointSizePixels = 1 if plot_all_protons else 2
    PseudocolorAtts.renderSurfaces = 1
    PseudocolorAtts.renderWireframe = 0
    PseudocolorAtts.renderPoints = 0
    PseudocolorAtts.smoothingLevel = 0
    PseudocolorAtts.legendFlag = 1
    PseudocolorAtts.lightingFlag = 1
    SetPlotOptions(PseudocolorAtts)


DrawPlots()
setCam()
setAtts()
InvertBackgroundColor()
DrawPlots()
saveWin('protons_vs_beam_2d_-%04d.png'%(protonbeam_it))

# Query the beam plot for the required data
SetActivePlots(2)
SetActivePlots(2)
SetActivePlots(2)
recordStats("Proton-protonbeam/ke", actual=1)
recordMinMax("Proton-protonbeam/ke", actual=1)
SetActivePlots(1)
SetActivePlots(1)
SetActivePlots(1)
recordMinMax("Proton-1/ke", actual=1)

DeleteAllPlots()
