
try:
    histke_pb_it
except NameError:
    histke_pb_it = 0
else:
    histke_pb_it += 1

numBinsHist = 512

#------------Plot 1: Histogram of ke of all protons
AddPlot("Histogram", "Proton-0/ke", 1, 0)

HistogramAtts = HistogramAttributes()
HistogramAtts.basedOn = HistogramAtts.ManyZonesForSingleVar  # ManyVarsForSingleZone, ManyZonesForSingleVar
HistogramAtts.histogramType = HistogramAtts.Frequency  # Frequency, Weighted, Variable
HistogramAtts.weightVariable = "default"
HistogramAtts.limitsMode = HistogramAtts.OriginalData  # OriginalData, CurrentPlot
HistogramAtts.minFlag = 0
HistogramAtts.maxFlag = 0
HistogramAtts.min = 0
HistogramAtts.max = 1
HistogramAtts.numBins = numBinsHist
HistogramAtts.domain = 0
HistogramAtts.zone = 0
HistogramAtts.useBinWidths = 1
HistogramAtts.outputType = HistogramAtts.Curve  # Curve, Block
HistogramAtts.lineStyle = HistogramAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
HistogramAtts.lineWidth = 3
HistogramAtts.color = (0, 0, 255, 255)
HistogramAtts.dataScale = HistogramAtts.Log  # Linear, Log, SquareRoot
HistogramAtts.binScale = HistogramAtts.Linear  # Linear, Log, SquareRoot
HistogramAtts.normalizeHistogram = 0
HistogramAtts.computeAsCDF = 0
SetPlotOptions(HistogramAtts)

DrawPlots()

# save the data
ExportDBAtts = ExportDBAttributes()
ExportDBAtts.allTimes = 0
ExportDBAtts.db_type = "XYZ"
ExportDBAtts.db_type_fullname = "XYZ_1.0"
ExportDBAtts.filename = '%s-beam-hist-ke-2d-%04d'%("Proton-0", histke_pb_it)
ExportDatabase(ExportDBAtts)

#---------------Plot 2: Histogram of ke of beam protons
# Query the first histogram to get min/max bounds we should use
SetQueryFloatFormat("%g")
SetQueryOutputToValue()
varMin = Query("Min", use_actual_data=False)
varMax = Query("Max", use_actual_data=False)
# Add the histogram for just the beam particles
AddPlot("Histogram", "Proton-protonbeam/ke", 1, 0)

HistogramAtts = HistogramAttributes()
HistogramAtts.basedOn = HistogramAtts.ManyZonesForSingleVar  # ManyVarsForSingleZone, ManyZonesForSingleVar
HistogramAtts.histogramType = HistogramAtts.Frequency  # Frequency, Weighted, Variable
HistogramAtts.weightVariable = "default"
HistogramAtts.limitsMode = HistogramAtts.OriginalData  # OriginalData, CurrentPlot
HistogramAtts.minFlag = 1
HistogramAtts.maxFlag = 1
HistogramAtts.min = varMin
HistogramAtts.max = varMax
HistogramAtts.numBins = numBinsHist
HistogramAtts.domain = 0
HistogramAtts.zone = 0
HistogramAtts.useBinWidths = 1
HistogramAtts.outputType = HistogramAtts.Curve  # Curve, Block
HistogramAtts.lineStyle = HistogramAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
HistogramAtts.lineWidth = 1
HistogramAtts.color = (255, 0, 0, 200)
HistogramAtts.dataScale = HistogramAtts.Log  # Linear, Log, SquareRoot
HistogramAtts.binScale = HistogramAtts.Linear  # Linear, Log, SquareRoot
HistogramAtts.normalizeHistogram = 0
HistogramAtts.computeAsCDF = 0
SetPlotOptions(HistogramAtts)

DrawPlots()

# Save the figure with the two curves
saveWin('beam-hist-ke-compare-%04d.png'%(histke_pb_it), 1920, 1080)

# Delete the first histogram plot so that we can export the xyz file for the second curve
SetActivePlots(0)
DeleteActivePlots()
SetActivePlots(0)

# save the data for protonbeam data
ExportDBAtts = ExportDBAttributes()
ExportDBAtts.allTimes = 0
ExportDBAtts.db_type = "XYZ"
ExportDBAtts.db_type_fullname = "XYZ_1.0"
ExportDBAtts.filename = '%s-beam-hist-ke-2d-%04d'%("Proton-protonbeam", histke_pb_it)
ExportDatabase(ExportDBAtts)

DeleteAllPlots()
