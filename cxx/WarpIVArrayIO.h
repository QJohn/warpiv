#ifndef WarpIVArrayIO_h
#define WarpIVArrayIO_h

#ifdef BUILD_WITHOUT_MPI
#define MPI_FLOAT 0
#define MPI_DOUBLE 0
#define MPI_CHAR 0
#define MPI_UNSIGNED_CHAR 0
#define MPI_SHORT 0
#define MPI_UNSIGNED_SHORT 0
#define MPI_INT 0
#define MPI_UNSIGNED_INT 0
#define MPI_UNSIGNED 0
#define MPI_LONG 0
#define MPI_UNSIGNED_LONG 0
#define MPI_LONG_LONG 0
#define MPI_UNSIGNED_LONG_LONG 0
typedef void* MPI_Status;
typedef void* MPI_Datatype;
typedef void* MPI_Info;
typedef void* MPI_File;
typedef void* MPI_Comm;
typedef void* MPI_Info;
#else
#include <mpi.h>
#endif

#ifdef BUILD_WITHOUT_CARTESIAN_EXTENT
typedef int* WarpIVExtent;
#else
#include "WarpIVExtent.h"
#endif

// *****************************************************************************
template <typename T>
size_t size(const T &ext)
{
  size_t n = 1;
  for (int i = 0; i < 3; ++i)
    n *= ext[2*i+1] - ext[2*i] + 1;
  return n;
}

// *****************************************************************************
template <typename T>
void size(const T &ext, int *n)
{
  for (int i = 0; i < 3; ++i)
    n[i] = ext[2*i+1] - ext[2*i] + 1;
}

// *****************************************************************************
template <typename T>
void start(const T &ext, int *id)
{
  for (int i = 0; i < 3; ++i)
    id[i] = ext[2*i];
}

// *****************************************************************************
template <typename T>
void offset(const T &dom, const T &subdom, int *id)
{
  for (int i = 0; i < 3; ++i)
    id[i] = dom[2*i] - subdom[2*i];
}

// *****************************************************************************
template <typename T>
bool equal(const T &l, const T &r)
{
  for (int i = 0; i < 6; ++i)
  {
    if (l[i] != r[i])
      return false;
  }
  return true;
}

// *****************************************************************************
template < typename T> struct DataTraits;
template <> struct DataTraits < float> { static MPI_Datatype Type(){ return MPI_FLOAT; } };
template <> struct DataTraits < double> { static MPI_Datatype Type(){ return MPI_DOUBLE; } };
template <> struct DataTraits < short int> { static MPI_Datatype Type(){ return MPI_SHORT; } };
template <> struct DataTraits < unsigned short int> { static MPI_Datatype Type(){ return MPI_UNSIGNED_SHORT; } };
template <> struct DataTraits < int> { static MPI_Datatype Type(){ return MPI_INT; } };
template <> struct DataTraits < unsigned int> { static MPI_Datatype Type(){ return MPI_UNSIGNED; } };
template <> struct DataTraits < long> { static MPI_Datatype Type(){ return MPI_LONG; } };
template <> struct DataTraits < unsigned long> { static MPI_Datatype Type(){ return MPI_UNSIGNED_LONG; } };
template <> struct DataTraits < long long> { static MPI_Datatype Type(){ return MPI_LONG_LONG; } };
template <> struct DataTraits < unsigned long long> { static MPI_Datatype Type(){ return MPI_UNSIGNED_LONG_LONG; } };
template <> struct DataTraits < signed char> { static MPI_Datatype Type(){ return MPI_CHAR; } };
template <> struct DataTraits < char> { static MPI_Datatype Type(){ return MPI_CHAR; } };
template <> struct DataTraits < unsigned char> { static MPI_Datatype Type(){ return MPI_UNSIGNED_CHAR; } };

// ****************************************************************************
template <typename T>
void createCartesianView(
      int domain[6],
      int decomp[6],
      MPI_Datatype &view)
{
  #ifdef BUILD_WITHOUT_MPI
  (void)domain;
  (void)decomp;
  (void)view;
  #else
  int mpiOk = 0;
  MPI_Initialized(&mpiOk);
  if (!mpiOk)
  {
    std::cerr << "This class requires the MPI runtime" << std::endl;
    return;
  }

  MPI_Datatype nativeType = DataTraits <T>::Type();

  int domainDims[3];
  size(domain, domainDims);
  int domainStart[3];
  start(domain, domainStart);

  int decompDims[3];
  size(decomp, decompDims);
  int decompStart[3];
  start(decomp, decompStart);

  // use a contiguous type when possible.
  if (equal(domain, decomp))
  {
    if (MPI_Type_contiguous(size(decomp), nativeType, &view))
    {
      std::cerr << "MPI_Type_contiguous failed." << std::endl;
    }
  }
  else
  {
    if (MPI_Type_create_subarray(3, domainDims,
        decompDims, decompStart, MPI_ORDER_FORTRAN, nativeType, &view))
    {
      std::cerr << "MPI_Type_create_subarray failed." << std::endl;
    }
  }
  if (MPI_Type_commit(&view))
  {
    std::cerr << "MPI_Type_commit failed." << std::endl;
  }
  #endif
}

// ****************************************************************************
template < typename T>
void createCartesianView(
      int domain[6],
      int decomp[6],
      int nComps,
      MPI_Datatype &view)
{
  #ifdef BUILD_WITHOUT_MPI
  (void)domain;
  (void)decomp;
  (void)nComps;
  (void)view;
  #else
  int mpiOk = 0;
  MPI_Initialized(&mpiOk);
  if (!mpiOk)
  {
    std::cerr << "This class requires the MPI runtime" << std::endl;
    return;
  }

  MPI_Datatype nativeType;
  if (MPI_Type_contiguous(nComps, DataTraits < T>::Type(),&nativeType))
  {
    std::cerr << "MPI_Type_contiguous failed." << std::endl;
  }

  int domainDims[3];
  size(domain, domainDims);
  int domainStart[3];
  start(domain, domainStart);

  int decompDims[3];
  size(decomp, decompDims);
  int decompStart[3];
  start(decomp, decompStart);

  // use a contiguous type when possible.
  if (equal(domain, decomp))
  {
    if (MPI_Type_contiguous(size(decomp), nativeType, &view))
    {
      std::cerr << "MPI_Type_contiguous failed." << std::endl;
    }
  }
  else
  {
    if (MPI_Type_create_subarray(3, domainDims, decompDims,
        decompStart, MPI_ORDER_FORTRAN, nativeType, &view))
    {
      std::cerr << "MPI_Type_create_subarray failed." << std::endl;
    }
  }
  if (MPI_Type_commit(&view))
  {
    std::cerr << "MPI_Type_commit failed." << std::endl;
  }

  MPI_Type_free(&nativeType);
  #endif
}

// ****************************************************************************
template < typename T>
int writeArray(
        const char *fileName,          // File name to write.
        MPI_Comm comm,                 // MPI communicator handle
        MPI_Info hints,                // MPI file hints
        int domain[6],                 // entire region, dataset extents
        int decomp[6],                 // local memory region, block extents with ghost zones
        int valid[6],                  // region to write to disk
        T *data)                       // pointer to a buffer to write to disk.
{
  int iErr = 0;
  #ifdef BUILD_WITHOUT_MPI
  (void)fileName;
  (void)comm;
  (void)hints;
  (void)domain;
  (void)decomp;
  (void)data;
  #else
  int mpiOk = 0;
  MPI_Initialized(&mpiOk);
  if (!mpiOk)
  {
    std::cerr << "This class requires the MPI runtime" << std::endl;
    return -1;
  }

  const int eStrLen = 2048;
  char eStr[eStrLen] = {'\0'};
  // Open the file
  MPI_File file;
  if ((iErr = MPI_File_open(comm, const_cast<char *>(fileName),
      MPI_MODE_WRONLY|MPI_MODE_CREATE, MPI_INFO_NULL, &file)))
  {
    MPI_Error_string(iErr, eStr, const_cast<int *>(&eStrLen));
    std::cerr << "Error opeing file: " << fileName << std::endl
        << eStr << std::endl;
    return -1;
  }

  // calculate block offsets and lengths
  int domainDims[3];
  size(domain, domainDims);

  int decompDims[3];
  size(decomp, decompDims);

  int validDims[3];
  size(valid, validDims);

  int validStart[3];
  start(valid, validStart);

  int validOffset[3];
  offset(decomp, valid, validOffset);

  // file view
  MPI_Datatype nativeType = DataTraits<T>::Type();
  MPI_Datatype fileView;
  if (MPI_Type_create_subarray(3, domainDims,
      validDims, validStart, MPI_ORDER_FORTRAN,
      nativeType, &fileView))
  {
    std::cerr << "MPI_Type_create_subarray failed." << std::endl;
    return -1;
  }

  if (MPI_Type_commit(&fileView))
  {
    std::cerr << "MPI_Type_commit failed." << std::endl;
    return -1;
  }

  if (MPI_File_set_view(file, 0,
      nativeType, fileView, "native", hints))
  {
    std::cerr << "MPI_File_set_view failed." << std::endl;
    return -1;
  }

  // memory view
  MPI_Datatype memView;
  if (MPI_Type_create_subarray(3, decompDims,
      validDims, validOffset, MPI_ORDER_FORTRAN,
      nativeType, &memView))
  {
      std::cerr << "MPI_Type_create_subarray failed." << std::endl;
      return -1;
  }

  if (MPI_Type_commit(&memView))
  {
    std::cerr << "MPI_Type_commit failed." << std::endl;
    return -1;
  }

  // write
  MPI_Status status;
  if ((iErr = MPI_File_write_all(file, data, 1, memView, &status)))
  {
    MPI_Error_string(iErr, eStr, const_cast<int *>(&eStrLen));
    std::cerr << "Error writing file: " << fileName << std::endl
        << eStr << std::endl;
    return -1;
  }

  MPI_File_close(&file);
  MPI_Type_free(&fileView);
  MPI_Type_free(&memView);
  #endif
  return iErr ? -1 : 0;
}

/**
NOTE: The file is a scalar, but the memory can be vector
where file elements are seperated by a constant stride.
WARNING it's very slow to read into a strided array if
data seiving is off, or seive buffer is small.
*/
// ****************************************************************************
template <typename T>
int readArray(
        const char *fileName,           // File name to read.
        MPI_Comm comm,                  // MPI communicator handle
        MPI_Info hints,                 // MPI file hints
        int domain[6],                  // entire region, dataset extents
        int decomp[6],                  // region to be read, block extents
        int nCompsMem,                  // stride in memory array
        int compNoMem,                  // start offset in mem array
        T *data)                        // pointer to a buffer to read into.
{
  int iErr = 0;
  #ifdef BUILD_WITHOUT_MPI
  (void)fileName;
  (void)comm;
  (void)hints;
  (void)domain;
  (void)decomp;
  (void)nCompsMem;
  (void)compNoMem;
  (void)data;
  #else
  int mpiOk = 0;
  MPI_Initialized(&mpiOk);
  if (!mpiOk)
  {
    std::cerr << "This class requires the MPI runtime" << std::endl;
    return -1;
  }

  int eStrLen = 256;
  char eStr[256] = {'\0'};
  // Open the file
  MPI_File file;
  if (MPI_File_open(comm, const_cast<char *>(fileName),
      MPI_MODE_RDONLY, hints, &file))
  {
    MPI_Error_string(iErr, eStr, const_cast<int *>(&eStrLen));
    std::cerr << "Error opeing file: " << fileName << std::endl
        << eStr << std::endl;
    return -1;
  }

  // Locate our data.
  int domainDims[3];
  size(domain, domainDims);

  int decompDims[3];
  size(decomp, decompDims);
  int decompStart[3];
  start(decomp, decompStart);

  // file view
  MPI_Datatype nativeType = DataTraits < T>::Type();
  MPI_Datatype fileView;
  if (MPI_Type_create_subarray(3, domainDims, decompDims,
      decompStart, MPI_ORDER_FORTRAN, nativeType, &fileView))
  {
    std::cerr << "MPI_Type_create_subarray failed." << std::endl;
  }
  if (MPI_Type_commit(&fileView))
  {
    std::cerr << "MPI_Type_commit failed." << std::endl;
  }
  if (MPI_File_set_view(file, 0, nativeType, fileView, "native", hints))
  {
    std::cerr << "MPI_File_set_view failed." << std::endl;
  }

  // memory view.
  MPI_Datatype memView;
  int n = size(decomp);
  if (nCompsMem == 1)
  {
    if (MPI_Type_contiguous(n, nativeType, &memView))
    {
      std::cerr << "MPI_Type_contiguous failed." << std::endl;
    }
  }
  else
  {
    if (MPI_Type_vector(n, 1, nCompsMem, nativeType, &memView))
    {
      std::cerr << "MPI_Type_vector failed." << std::endl;
    }
  }
  if (MPI_Type_commit(&memView))
  {
    std::cerr << "MPI_Type_commit failed." << std::endl;
  }

  // Read
  MPI_Status status;
  if ((iErr = MPI_File_read_all(file, data+compNoMem, 1, memView, &status)))
  {
    MPI_Error_string(iErr, eStr, &eStrLen);
    std::cerr << "Error reading file: " << fileName << std::endl
        << eStr << std::endl;
  }
  MPI_Type_free(&fileView);
  MPI_Type_free(&memView);
  MPI_File_close(&file);
  #endif
  return iErr ? -1 : 0;
}

/**
NOTE: The file is always a scalar, but the memory can be vector
where file elements are seperated by a constant stride.
it's very slow to write from/read into a strided array
better buffer above.
*/
// ****************************************************************************
template <typename T>
int writeArray(
        MPI_File file,              // MPI file handle
        MPI_Info hints,             // MPI file hints
        int domain[6],              // entire region, dataset extents
        int decomp[6],              // region to be read, block extents
        int nCompsMem,              // stride in memory array
        int compNoMem,              // start offset in mem array
        T *data)                    // pointer to a buffer to write from.
{
  int iErr = 0;
  #ifdef BUILD_WITHOUT_MPI
  (void)file;
  (void)hints;
  (void)domain;
  (void)decomp;
  (void)nCompsMem;
  (void)compNoMem;
  (void)data;
  #else
  int mpiOk = 0;
  MPI_Initialized(&mpiOk);
  if (!mpiOk)
  {
    std::cerr << "This class requires the MPI runtime" << std::endl;
    return -1;
  }

  int eStrLen = 256;
  char eStr[256] = {'\0'};

  // Locate our data.
  int domainDims[3];
  size(domain, domainDims);

  int decompDims[3];
  size(decomp, decompDims);

  int decompStart[3];
  start(decomp, decompStart);

  // file view
  MPI_Datatype nativeType = DataTraits < T>::Type();
  MPI_Datatype fileView;
  if (MPI_Type_create_subarray(3, domainDims, decompDims,
      decompStart, MPI_ORDER_FORTRAN, nativeType, &fileView))
  {
    std::cerr << "MPI_Type_create_subarray failed." << std::endl;
  }
  if (MPI_Type_commit(&fileView))
  {
    std::cerr << "MPI_Type_commit failed." << std::endl;
  }
  if (MPI_File_set_view(file, 0, nativeType, fileView, "native", hints))
  {
    std::cerr << "MPI_File_set_view failed." << std::endl;
  }

  // memory view.
  MPI_Datatype memView;
  int n = size(decomp);
  if (nCompsMem == 1)
  {
    if (MPI_Type_contiguous(n, nativeType, &memView))
    {
      std::cerr << "MPI_Type_contiguous failed." << std::endl;
    }
  }
  else
  {
    if (MPI_Type_vector(n, 1, nCompsMem, nativeType, &memView))
    {
      std::cerr << "MPI_Type_vector failed." << std::endl;
    }
  }
  MPI_Type_commit(&memView);
  {
    std::cerr << "MPI_Type_commit failed." << std::endl;
  }

  // write
  MPI_Status status;
  if ((iErr = MPI_File_write_all(file, data+compNoMem, 1, memView, &status)))
  {
    MPI_Error_string(iErr, eStr, &eStrLen);
    std::cerr << "Error writing file." << std::endl
        << eStr << std::endl;
  }
  MPI_Type_free(&fileView);
  MPI_Type_free(&memView);
  #endif
  return iErr ? -1 : 0;
}

// ****************************************************************************
template <typename T>
int readArray(
        MPI_File file,              // MPI file handle
        MPI_Info hints,             // MPI file hints
        int domain[6],              // entire region, dataset extents
        int decomp[6],              // region to be read, block extents
        int nCompsMem,              // stride in memory array
        int compNoMem,              // start offset in mem array
        T *data)                    // pointer to a buffer to read into.
{
  int iErr = 0;
  #ifdef BUILD_WITHOUT_MPI
  (void)file;
  (void)hints;
  (void)domain;
  (void)decomp;
  (void)nCompsMem;
  (void)compNoMem;
  (void)data;
  #else
  int mpiOk = 0;
  MPI_Initialized(&mpiOk);
  if (!mpiOk)
  {
    std::cerr << "This class requires the MPI runtime" << std::endl;
    return 0;
  }

  int eStrLen = 256;
  char eStr[256] = {'\0'};

  // Locate our data.
  int domainDims[3];
  size(domain, domainDims);
  int domainStart[3];
  start(domain, domainStart);

  int decompDims[3];
  size(decomp, decompDims);
  int decompStart[3];
  start(decomp, decompStart);

  // file view
  MPI_Datatype nativeType = DataTraits < T>::Type();
  MPI_Datatype fileView;
  if (MPI_Type_create_subarray(3, domainDims, decompDims,
      decompStart, MPI_ORDER_FORTRAN, nativeType, &fileView))
  {
    std::cerr << "MPI_Type_create_subarray failed." << std::endl;
  }
  if (MPI_Type_commit(&fileView))
  {
    std::cerr << "MPI_Type_commit failed." << std::endl;
  }
  if (MPI_File_set_view(file, 0, nativeType, fileView, "native", hints))
  {
    std::cerr << "MPI_File_set_view failed." << std::endl;
  }

  // memory view.
  MPI_Datatype memView;
  int n = size(decomp);
  if (nCompsMem == 1)
  {
    if (MPI_Type_contiguous(n, nativeType, &memView))
    {
      std::cerr << "MPI_Type_contiguous failed." << std::endl;
    }
  }
  else
  {
    if (MPI_Type_vector(n, 1, nCompsMem, nativeType, &memView))
    {
      std::cerr << "MPI_Type_vector failed." << std::endl;
    }
  }
  if (MPI_Type_commit(&memView))
  {
    std::cerr << "MPI_Type_commit failed." << std::endl;
  }

  // Read
  MPI_Status status;
  if ((iErr = MPI_File_read_all(file, data+compNoMem, 1, memView, &status)))
  {
    MPI_Error_string(iErr, eStr, &eStrLen);
    std::cerr << "Error reading file." << std::endl
        << eStr << std::endl;
  }
  MPI_Type_free(&fileView);
  MPI_Type_free(&memView);
  #endif
  return iErr ? -1 : 0;
}

/**
Read the from the region defined by the file view into the
region defined by the memory veiw.
*/
// ****************************************************************************
template <typename T>
int readArray(
        MPI_File file,         // MPI file handle
        MPI_Info hints,        // MPI file hints
        MPI_Datatype memView,  // memory region
        MPI_Datatype fileView, // file layout
        T *data)               // pointer to a buffer to read into.
{
  #ifdef BUILD_WITHOUT_MPI
  (void)file;
  (void)hints;
  (void)memView;
  (void)fileView;
  (void)data;
  #else
  int mpiOk = 0;
  MPI_Initialized(&mpiOk);
  if (!mpiOk)
  {
    std::cerr << "This class requires the MPI runtime" << std::endl;
    return 0;
  }

  int iErr;
  int eStrLen = 256;
  char eStr[256] = {'\0'};

  MPI_Datatype nativeType = DataTraits < T>::Type();
  iErr = MPI_File_set_view(
      file, 0, nativeType, fileView, "native", hints);

  MPI_Status status;
  if ((iErr = MPI_File_read_all(file, data, 1, memView, &status)))
  {
    MPI_Error_string(iErr, eStr, &eStrLen);
    std::cerr << "Error reading file." << std::endl << eStr << std::endl;
    return -1;
  }
  #endif
  return 0;
}

// ****************************************************************************
enum
{
  HINT_DEFAULT = 0,
  HINT_AUTOMATIC = 0,
  HINT_DISABLED = 1,
  HINT_ENABLED = 2
};

// ****************************************************************************
MPI_Info createWriterHints(
    int useCollectiveIO = HINT_ENABLED,
    int numberOfIONodes = -1,
    int collectiveBufferSize = -1,
    int useDirectIO = HINT_AUTOMATIC,
    int useDefferedOpen = HINT_AUTOMATIC,
    int useDataSeive = HINT_AUTOMATIC,
    int sieveBufferSize = -1,
    int stripeCount = -1,
    int stripeSize = -1);

#endif
