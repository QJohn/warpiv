import matplotlib.pyplot as plt
import numpy as np
import sys

def fillhist(bns, cnts, c, a):
    # bars
    dx = bns[1] - bns[0]
    plt.bar(bns, cnts, dx, edgecolor=c, alpha=a)
    # outline
    n = len(bns)
    i = 0
    while i < n:
        plt.plot([bns[i], bns[i]+dx], [cnts[i], cnts[i]],c,linewidth=2)
        if i < n-1:
            plt.plot([bns[i]+dx, bns[i]+dx], [cnts[i], cnts[i+1]],'k',linewidth=2)
        i += 1

def normalize(bns, cnts):
    # compute area
    dx = bns[1] - bns[0]
    i = 0
    A = 0.0
    n = len(bns)
    while i < n:
        A += dx+bns[i]*cnts[i] # TODO -- I think it should be dx*cnt
        i += 1
    # normalize by area
    n_cnts = cnts/A
    return n_cnts

def loadxyz(fn, norm=True):
    f = open(fn)
    ls = f.readlines()
    bns = []
    cnts = []
    for l in ls[2:]:
        x,bn,cnt,y = l.split()
        bns.append(float(bn))
        cnts.append(int(cnt))

    bns = np.array(bns)
    cnts = np.array(cnts)

    if norm:
        cnts = normalize(bns, cnts)

    return bns, cnts

def fillplot(x, y, c, a):
    plt.fill_between(x, y, color=c, alpha=a)
    plt.plot(x, y, ':',color=c, linewidth=3, alpha=0.45*a)
    h = plt.Rectangle((0, 0), 1, 1, fc=c, alpha=a)
    return h

def clearplot(x, y):
    plt.fill_between(x, y, color='w', alpha=1.0)

def loadt(fn, fpt=True):
    f = open(fn)
    ls = f.readlines()
    t = []
    for l in ls:
        if fpt:
            t.append(float(l))
        else:
            t.append(int(l))
    return t

#plt.figure()
#histfill(bns, cnts, 'b', 0.5)

#plt.figure()
#histfill(bns, n_cnts, 'b', 0.5)

its = loadt('it.dat', False)
tm = loadt('t.dat')

q = 0
while q < 60:
    try:
        e_b, e_c = loadxyz('Electron-All-hist-ke-2d-%04d.xyz'%(q))
        c_b, c_c = loadxyz('Carbon-0-hist-ke-2d-%04d.xyz'%(q))
        p_b, p_c = loadxyz('Proton-0-hist-ke-2d-%04d.xyz'%(q))

        fig = plt.figure()
        e_h = fillplot(e_b, e_c, 'b', 0.45)
        clearplot(c_b, c_c)
        c_h = fillplot(c_b, c_c, 'g', 0.45)
        clearplot(p_b, p_c)
        p_h = fillplot(p_b, p_c, 'r', 0.45)
        ax = fig.gca()
        ax.set_axisbelow(True)
        ax.set_ylim([0, 0.006])
        plt.grid(True)
        plt.xlabel('log ke', fontweight='bold', fontsize=12)
        plt.ylabel('fraction', fontweight='bold', fontsize=12)
        plt.title('ke distribution it=%d t=%g'%(its[q], tm[q]), fontweight='bold', fontsize=14)
        plt.legend([e_h, c_h, p_h], ['Electron', 'Carbon', 'Proton'])

        plt.savefig('ke-dist-%04d.png'%(q))

        plt.close(fig)

    except:
        sys.stderr.write('an error occured at %d\n'%(q))

    q += 1
