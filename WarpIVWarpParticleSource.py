import numpy as np
from WarpIVUtil import pError,pDebug,pStatus
import warpoptions
warpoptions.ignoreUnknownArgs = True
warpoptions.quietImport = True
warpoptions.init_parser()
import warp
import parallel

class WarpIVWarpParticleSource(object):
    """
    Data API class for mesh based objects. The data API generalizes
    the interface needed for the vis backend to access the data. This
    implementation handles particle based data from Warp.

    TODO -- do we need a base class to formalize the API??
    TODO -- factory class
    TODO -- WarpIVOpenPMDMeshData , for reading OpenPMD say from
            a burst buffer
    """
    #-----------------------------------------------------------------------------
    def __init__(self):
        """
        construct the particle source.
        """
        return

    #-----------------------------------------------------------------------------
    def GetNumberOfDomains(self):
        """ return the number of domains total """
        return parallel.number_of_PE()

    #-----------------------------------------------------------------------------
    def GetDomainIds(self):
        """return the list of domain ids local to this rank """
        return [parallel.get_rank()]

    #-----------------------------------------------------------------------------
    def GetCoordinates(self, name):
        """ return x,y,z coords """
        species = self.GetSpecies(name)
        x = species.getx(gather=0)
        y = species.gety(gather=0)
        z = species.getz(gather=0)
        return [x,y,z]

    #-----------------------------------------------------------------------------
    def GetArrayNames(self):
        pVarNames = ['pid','n','x','y','z','w','r',
            'theta','vtheta','etheta','btheta',
            'vx','vy','vz','vr','ux','uy','uz',
            'ex','ey','ez','er','bx','by','bz','br',
            'xp','yp','zp','rp','tp','gaminv','weights',
            'ke','MPI_rank']
        pVarNames.sort()
        return pVarNames

    #-----------------------------------------------------------------------------
    def GetArray(self, meshName, varName):
        """
        Get an array of data by name
        """
        species = self.GetSpecies(meshName)

        # mpi rank is a special case we added for debugging
        if varName == 'MPI_rank':
            n = species.getx(gather=0).size
            if not n:
                return None
            rank = np.empty([n], dtype=float, order='F')
            rank.fill(float(parallel.get_rank()))
            return rank

        # for all other vars on particle mesh
        getFunc = getattr(species,'get' + varName)
        return getFunc(gather=0)

    #-----------------------------------------------------------------------------
    def GetMeshName(self, species, speciesEnum):
        """Given a species construct a mesh name"""
        meshName = species.type.name
        if (species.name is None) or (species.name==''):
            if not speciesEnum.has_key(species.type.name):
                speciesEnum[species.type.name] = 0
            meshName += '-%d'%(speciesEnum[species.type.name])
            speciesEnum[species.type.name] += 1
        else:
            meshName += '-' + species.name
        return meshName

    #-----------------------------------------------------------------------------
    def GetMeshNames(self):
        """Get list of particle meshes from warp"""
        # see: warp/scripts/species.py
        # a mesh for each species
        meshNames = []
        speciesEnum = {}
        for species in warp.listofallspecies:
            meshNames.append(self.GetMeshName(species, speciesEnum))
        return meshNames

    #-----------------------------------------------------------------------------
    def GetSpecies(self, meshName):
        """Given a mesh name locate the spceies object for it"""
        meshNames = self.GetMeshNames()
        if meshNames.count(meshName) > 0:
            return warp.listofallspecies[meshNames.index(meshName)]
        else:
            pError('Could not find species for mesh %s'%(meshName))
        return None
