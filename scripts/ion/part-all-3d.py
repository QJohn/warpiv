
# counter for time step out
try:
    p3a_it
except NameError:
    p3a_it = 0
else:
    p3a_it += 1

spec = ['Electron-All', 'Carbon-0', 'Proton-0']
expr = ['Electron-All/ke', 'Carbon-0/ke', 'Proton-0/ke']
lut = ['electron_', 'carbon_', 'proton_']
var = ['ke']
varOut = ['ke']

j = 0
nj = len(var)
while j < nj:
    AddPlot("Pseudocolor", "grid/Ey", 1, 0)
    DrawPlots()
    rng = recordMinMax("Ey", 0)
    lim = max(abs(rng[0]), abs(rng[1]))

    AddOperator("Isosurface", 0)
    IsosurfaceAtts = IsosurfaceAttributes()
    IsosurfaceAtts.contourNLevels = 20
    IsosurfaceAtts.contourValue = ()
    IsosurfaceAtts.contourPercent = (1, 5, 10, 15, 20, 25, 30, 75, 80, 85, 90, 95, 99)
    IsosurfaceAtts.contourMethod = IsosurfaceAtts.Level  # Level, Value, Percent
    IsosurfaceAtts.minFlag = 0
    IsosurfaceAtts.min = -lim
    IsosurfaceAtts.maxFlag = 0
    IsosurfaceAtts.max = lim
    IsosurfaceAtts.scaling = IsosurfaceAtts.Linear  # Linear, Log
    IsosurfaceAtts.variable = "default"
    SetOperatorOptions(IsosurfaceAtts, 0)

    clipQuadrant1()

    PseudocolorAtts = PseudocolorAttributes()
    PseudocolorAtts.scaling = PseudocolorAtts.Linear  # Linear, Log, Skew
    PseudocolorAtts.skewFactor = 1
    PseudocolorAtts.limitsMode = PseudocolorAtts.CurrentPlot  # OriginalData, CurrentPlot
    PseudocolorAtts.minFlag = 0
    PseudocolorAtts.min = -lim
    PseudocolorAtts.maxFlag = 1
    PseudocolorAtts.max = lim
    PseudocolorAtts.centering = PseudocolorAtts.Natural  # Natural, Nodal, Zonal
    PseudocolorAtts.colorTableName = "sym_gray"
    PseudocolorAtts.invertColorTable = 0
    PseudocolorAtts.opacityType = PseudocolorAtts.ColorTable  # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
    PseudocolorAtts.opacityVariable = ""
    PseudocolorAtts.opacity = 1
    PseudocolorAtts.opacityVarMin = 0
    PseudocolorAtts.opacityVarMax = 1
    PseudocolorAtts.opacityVarMinFlag = 0
    PseudocolorAtts.opacityVarMaxFlag = 0
    PseudocolorAtts.pointSize = 0.05
    PseudocolorAtts.pointType = PseudocolorAtts.Point  # Box, Axis, Icosahedron, Octahedron, Tetrahedron, SphereGeometry, Point, Sphere
    PseudocolorAtts.pointSizeVarEnabled = 0
    PseudocolorAtts.pointSizeVar = "default"
    PseudocolorAtts.pointSizePixels = 2
    PseudocolorAtts.lineType = PseudocolorAtts.Line  # Line, Tube, Ribbon
    PseudocolorAtts.lineStyle = PseudocolorAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
    PseudocolorAtts.lineWidth = 0
    PseudocolorAtts.tubeDisplayDensity = 10
    PseudocolorAtts.tubeRadiusSizeType = PseudocolorAtts.FractionOfBBox  # Absolute, FractionOfBBox
    PseudocolorAtts.tubeRadiusAbsolute = 0.125
    PseudocolorAtts.tubeRadiusBBox = 0.005
    PseudocolorAtts.varyTubeRadius = 0
    PseudocolorAtts.varyTubeRadiusVariable = ""
    PseudocolorAtts.varyTubeRadiusFactor = 10
    PseudocolorAtts.endPointType = PseudocolorAtts.None  # None, Tails, Heads, Both
    PseudocolorAtts.endPointStyle = PseudocolorAtts.Spheres  # Spheres, Cones
    PseudocolorAtts.endPointRadiusSizeType = PseudocolorAtts.FractionOfBBox  # Absolute, FractionOfBBox
    PseudocolorAtts.endPointRadiusAbsolute = 1
    PseudocolorAtts.endPointRadiusBBox = 0.005
    PseudocolorAtts.endPointRatio = 2
    PseudocolorAtts.renderSurfaces = 1
    PseudocolorAtts.renderWireframe = 0
    PseudocolorAtts.renderPoints = 0
    PseudocolorAtts.smoothingLevel = 0
    PseudocolorAtts.legendFlag = 1
    PseudocolorAtts.lightingFlag = 1
    SetPlotOptions(PseudocolorAtts)

    i = 0
    ni = len(spec)
    while i < ni:
        active_spec = spec[i]

        AddPlot("Pseudocolor", active_spec + "/" + var[j], 1, 0)

        clipQuadrant1()

        active_lut = lut[i] + varOut[j]

        PseudocolorAtts = PseudocolorAttributes()
        PseudocolorAtts.minFlag = 1
        PseudocolorAtts.min = 0
        PseudocolorAtts.maxFlag = 0
        PseudocolorAtts.max = 100
        PseudocolorAtts.colorTableName = active_lut
        PseudocolorAtts.invertColorTable = 0
        PseudocolorAtts.opacityType = PseudocolorAtts.ColorTable  if varOut[j] == 'den' else PseudocolorAtts.Ramp # ColorTable, FullyOpaque, Constant, Ramp, VariableRange
        PseudocolorAtts.opacity = 0.5 if active_spec == 'Electron-All' else 1.0
        PseudocolorAtts.opacityVarMin = 0
        PseudocolorAtts.opacityVarMax = 1
        PseudocolorAtts.opacityVarMinFlag = 0
        PseudocolorAtts.opacityVarMaxFlag = 0
        SetPlotOptions(PseudocolorAtts)

        DrawPlots()

        i += 1

    setCam()
    setAtts(bg='w')

    DrawPlots()
    saveWin('CPE-part-%s-3d-%04d.png'%(varOut[j], p3a_it))

    DeleteAllPlots()

    j += 1
